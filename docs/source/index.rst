.. LinQ documentation master file, created by
   sphinx-quickstart on Wed Mar  7 11:30:49 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Altronix SDK's
===============

The Altronix SDK's are available in the following languages

.. container:: sdk-feature-container

   .. image:: _static/img/nodejs.png

   * Cloud or LAN

   * Secured with TLS

   * Free Support

   * Docs: :ref:`ref-linq-js`

.. container:: sdk-feature-container

   .. image:: _static/img/c.png

   * Light weight

   * Free Support

.. container:: sdk-feature-container

   .. image:: _static/img/c_sharp.png

   * Windows enviorments

   * Secure with TLS

   * Free Support

   * Docs: :ref:`ref-c-sharp`

Contents
========

.. toctree::
   :maxdepth: 2

   sdk/linqjs/linqjs.rst
   sdk/csharp/csharp.rst
   api/http/http.rst
   api/zmtp/zmtp.rst
