```csharp
// Create Altronix.LINQ.Listener Class instance
Listener atx = new Listener();

// Set user's callback for OnAlerts Event
atx.OnAlerts(UserCallback);

// Start polling the devices for alerts
atx.ListenAll(devices);
```
