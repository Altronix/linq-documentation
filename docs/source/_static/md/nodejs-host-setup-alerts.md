```javascript
linq.on("new", device => console.log("New device connected! [%s]", device.serial()))
    .on("alert", (device, alert) => console.log("Received alert from device!"))
    .on("error", error => console.log(error));
```
