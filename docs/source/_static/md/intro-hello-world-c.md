```c
zmtp_bridge* bridge = zmtp_bridge_create(&bridge_settings, NULL);
if (!bridge) return -1;

err = zmtp_bridge_listen(bridge, "tcp://127.0.0.1:9000");
if (err) {
    zmtp_bridge_destroy(&bridge);
    return err;
}

while (!err) err = zmtp_bridge_poll(bridge);
```
