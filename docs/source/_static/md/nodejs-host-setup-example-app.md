```javascript
/**
 * Basic app to print alerts to console
 */

let config = require("./config"),
  linq = require("@altronix/linq").linq;

// Setup event callback handlers
linq
  .on("error", e => console.log(e))
  .on("new", d => console.log("[%s] new device", d.serial()))
  .on("alert", (device, alert, email) => {
    console.log("[%s] alert", device.serial());
  })
  .listen(config).then(() => console.log("LinQ Ready!"))
  .catch(e => console.log(e);
```
