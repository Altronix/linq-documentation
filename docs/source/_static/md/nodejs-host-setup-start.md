```javascript
const config = {
  key: key,     // x509 TLS Private Key (optional)
  cert: cert,   // x509 TLS Public Certificate (optional)
  tcps: 99443,  // Secure Port for incoming secure device connections
  tcp: 9980     // Plain text port for incoming non secure device connections
};
linq.listen(config).then(() => console.log("LinQ Ready!"))
```
