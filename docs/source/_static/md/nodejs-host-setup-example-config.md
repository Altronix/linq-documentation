```javascript
let fs = require("fs");
const env = process.env.NODE_ENV || "dev";

// Read a potential TLS key from the enviorment
let key;
try {
  key = fs.readFileSync(process.env.TLS_KEY || "../enviorments/unsafe-key.pem");
  console.log("TLS KEY FOUND");
} catch (e) {
  console.log("No TLS key found");
}

// Read a potential TLS cert from the enviorment
let cert;
try {
  cert = fs.readFileSync(
    process.env.TLS_CERT || "../enviorments/unsafe-cert.pem"
  );
  console.log("TLS CERT FOUND");
} catch (e) {
  console.log("No TLS cert found");
}

const dev = {
  key: key,
  cert: cert,
  tcp: parseInt(process.env.TCP_PORT) || 3380,
  tcps: parseInt(process.env.TCPS_PORT) || 33433
};

const test = {
  key: key,
  cert: cert,
  tcp: parseInt(process.env.TCP_PORT) || 3380,
  tcps: parseInt(process.env.TCPS_PORT) || 33433
};

const config = { dev, test };

module.exports = config[env];
```
