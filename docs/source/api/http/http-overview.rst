Overview
========

The API is divided into two parts.

- Common API contain RESTful endpoints common to all Altronix LinQ enabled products

- Product API are unique RESTful endpoints per each product.  Product API's contain all of the common API plus the additional product specific API

Supported HTTPs Requests
------------------------

- GET: Retrieve a resource or collection of resources

- POST: Edit or Create a resource or collection of resources

- PUT: Same as POST. (--info) Note that resources are static and therefore PUT and POST are interpretted as the same)

- DELETE: Remove or zero a resource

Supported HTTPs Responses
-------------------------

- 200 (OK) The request was successful, new state was accepted

- 400 (Bad Request) The request was not successful.  The server rejected the request because of one of the following reasons
        
        - The request had bad format

- 403 (Unauthorized) The credentials supplied with the request were not valid. Or the credentials supplied with the request are valid, but the user does not have access to the requested resource.

- 404 (Not found) The resource endpoint does not exist

- 500 (Internal Server Error) The server cannot process the request at this time.

Supported Authentication Scheme
-------------------------------

All Altronix LinQ enabled products support Basic Authentication specification. Therefore the server expects the 'Authorization' header to be provided with each request requiring credentials.

::

| GET /ATX/userManagement/users\r\n
| Authorization: Basic QWxhZGRpbjpPcGVuU2VzYW1l\r\n
