Alert types
===========

.. http:get:: /ATX/alerts/types

   Returns a list of all the supported alert types for the LinQ2 product.

   **Example request**:

   .. sourcecode:: http

      GET /ATX/alerts/types HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "types": {
           "tmr": { "name": "tmr", "mesg": "Timer event trigger", "timer": 0 },
           "acFail": { "name": "acFail", "mesg": "AC input failure", "timer": 0 },
           "acFailR": { "name": "acFailR", "mesg": "AC input return", "timer": 0 },
           "battFail": { "name": "battFail", "mesg": "Battery fail", "timer": 0 },
           "battFailR": { "name": "battFailR", "mesg": "Battery return", "timer": 0 },
           "battServ": { "name": "battServ", "mesg": "Service Battery", "timer": 0 },
           "battServR": { "name": "battServR", "mesg": "Battery serviced", "timer": 0 },
           "psOn": { "name": "psOn", "mesg": "Power supply on", "timer": 0 },
           "psOff": { "name": "psOff", "mesg": "Power supply off", "timer": 0 },
           "ilow": { "name": "ilow", "mesg": "Under current", "timer": 0 },
           "ilowR": { "name": "ilowR", "mesg": "Under current clear", "timer": 0 },
           "ihigh": { "name": "ihigh", "mesg": "Over current", "timer": 0 },
           "ihighR": { "name": "ihighR", "mesg": "Over current clear", "timer": 0 },
           "rOn": { "name": "rOn", "mesg": "Relay engaged", "timer": 0 },
           "rOff": { "name": "rOff", "mesg": "Relay dis-engaged", "timer": 0 },
           "input": { "name": "input", "mesg": "Input trigger", "timer": 0 },
           "test": { "name": "test", "mesg": "this is an example alert", "timer": 0 },
           "sysUp": { "name": "sysUp", "mesg": "System Startup", "timer": 0 }
         }
      }


   :>json string tmr: The timer alert is recorded when a timer event expires
   :>json string acFail: The acFail alert is recorded when the AC failes below the eFlow specification AC input requirements
   :>json string acFailR: The acFailR alert is recorded when the AC recovers from an AC fail condition
   :>json string battFail: The battFail alert is recorded when the eFlow battery is presently installed but is reporting under voltage
   :>json string battFailR: The battFailR alert is recorded when the eFlow battery is presently installed and has recovered from an under voltage condition
   :>json string psOn: The psOn alert is recorded when a LinQ administrator turns on an eFlow Power Supply
   :>json string psOff: The psOff alert is recorded when a LinQ administrator turns off an eFlow Power Supply
   :>json string iLow: The iLow alert is recorded when a LinQ administrator has configured minimum load requirments, and the load to the eFlow Power Supply has dropped below limit the under current limit
   :>json string iLowR: The iLow alert is recorded when the eFlow Power Supply recoveres from an under current condition
   :>json string iHigh: The iHigh alert is recorded when a LinQ administrator has configured maximum load requirments, and the load to the eFlow Power Supply has exceeded the over current limit
   :>json string iHighR: The iHighR alert is recorded when the eFlow Power Supply recoveres from an over current condition
   :>json string rOn: The rOn alert is recorded when a LinQ administrator turns on a relay
   :>json string rOff: The rOff alert is recorded when a LinQ administrator turns off a relay
   :>json string input: The input alert is recorded when a LinQ2 module input trigger is engaged
   :>json string test: The test alert is recorded when a LinQ administrator activates a test alert. A test alert is used to simulate any of the other alert types and can be helpful when diagnosing alert notification configuration
   :>json string sysUp: The sysUp alert is recorded when the LinQ2 module first powers on (typically after a reboot or a power cycle event)
   :>json string name: The name of alert.  Is a KEY property to map alerts with a human readable message
   :>json string mesg: The message describing the alert.
   :>json number timer: A number in seconds used to filter alerts. When an alert condition only persists for less than x seconds, the alert will be filtered
