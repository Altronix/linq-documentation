Power supplies
==============

.. http:get:: /ATX/hardware/eflows/(string:index)

   Returns the state of the Eflow Power Supply at the respective index

   **Example request**:

   .. sourcecode:: http

      GET /ATX/hardware/eflows/eflow0 HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
        "eflow0": {
          "id": "Power Supply ID",
          "type": "",
          "psPresence": 1,
          "ps": 1,
          "pulse": -15,
          "status": "on",
          "ac": 0,
          "batt": 0,
          "vCal": 13863,
          "iCal": 4266,
          "v": 8000,
          "i": 13000,
          "vHigh": 28000,
          "iHigh": 5000,
          "iLow": 0,
          "service": 0,
          "reminder": 0,
          "battPresence": 1,
          "installation": 0
        }
      }

   :>json string id: The name of the resource (IE a relay or Eflow Power Supply can be named and are tagged with this name should an alert occur from this resource).
   :>json string type: The eflow type used to scale the voltage and current. IE: eFlow3NB
   :>json string psPresense: A boolean indicator to inform the system if the Eflow Power Supply is installed or not installed (1-installed, 0-not installed)
   :>json string ps: A boolean indicator to indicate if the Eflow Power Supply is on or off (1-on, 0-off)
   :>json number pulse: number of seconds to pulse the Eflow Power Supply, should a pulse command be executed. (<0 pulse off, >0 pulse on)
   :>json string status: Human readable string indicating the status of the Eflow Power Supply. (IE: "on"/"off")
   :>json number ac: Boolean indicator for AC status. (0 - AC fail, 1 - AC OK)
   :>json number batt: Boolean indicator for Battery status. (0 - Battery fail, 1 - Battery OK)
   :>json number vCal: TODO - calibration
   :>json number iCal: TODO - calibration
   :>json number v: Voltage level in (mV)
   :>json number i: Current level in (mA)
   :>json number vHigh: Over voltage threshold settings (mV)
   :>json number iHigh: Over Current threshold setting (mA)
   :>json number iLow: Under Current threshold setting (mA)
   :>json number service: Unix date code when battery service is expected
   :>json number reminder: Should battery serice reach expirey, an alert will be generated every [reminder] intervals (days)
   :>json number battPresence: Boolean indicator representing the presence of a battery per this Eflow Power Supply. (0 - no battery installed, 1 - battery installed)
   :>json number installation: Unix date code when the battery was installed, set by the administrator


.. http:post:: /ATX/hardware/eflows/(string:index)/status

   Change the state of the Eflow Power Supply output

   **Example request**:

   .. sourcecode:: http

      POST /ATX/hardware/eflows/eflow0/status HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript
      Content-Type: application/json

      {
         "status":0
      }

   :<json boolean status: If the supplied property is a number, the Eflow Power Supply will either turn on (status=1), or turn off (status=0)
   :<json string status: If the supplied property is a string, the Eflow Power Supply will either Pulse (status='p') or toggle (status='t'), Where a "pulse" command will either pulse on for x seconds, or pulse off for x seconds, and where seconds is determined by the "pulse" property of the same eflow object.

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "error":200
      }
