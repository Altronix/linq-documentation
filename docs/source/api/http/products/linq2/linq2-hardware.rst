Device IO summary
=================

.. http:get:: /ATX/hardware

   Returns a summary of the IO state of all hardware inputs, relays, and power supplies. For detailed information on JSON object properties, refer to the more explicit non-summary api, (IE: /ATX/hardware/eflows/eflow0)

   **Example request**:

   .. sourcecode:: http

      GET /ATX/hardware HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "hardware": {
           "exe": { "save": "application" },
           "eflows": {
             "eflow0": {
               "id": "Power Supply ID",
               "type": "",
               "psPresence": 1,
               "ps": 1,
               "pulse": -15,
               "status": "on",
               "ac": 0,
               "batt": 0,
               "vCal": 13863,
               "iCal": 4266,
               "v": 8000,
               "i": 13000,
               "vHigh": 28000,
               "iHigh": 5000,
               "iLow": 0,
               "service": 0,
               "reminder": 0,
               "battPresence": 1,
               "installation": 0,
               "timers": 500
             },
             "eflow1": {
               "id": "Power Supply ID",
               "type": "",
               "psPresence": 1,
               "ps": 1,
               "pulse": -15,
               "status": "on",
               "ac": 0,
               "batt": 0,
               "vCal": 13863,
               "iCal": 4266,
               "v": 7000,
               "i": 9000,
               "vHigh": 28000,
               "iHigh": 5000,
               "iLow": 0,
               "service": 0,
               "reminder": 0,
               "battPresence": 1,
               "installation": 0,
               "timers": 500
             },
             "mask": ""
           },
           "relays": {
             "relay0": { "id": "Relay ID", "status": 0, "pulse": -15 },
             "relay1": { "id": "Relay ID", "status": 0, "pulse": -15 },
             "mask": ""
           },
           "inputs": {
             "input0": { "id": "Input ID", "status": 0, "test": 0, "cmds": [] },
             "input1": { "id": "Input ID", "status": 0, "test": 0, "cmds": [] },
             "input2": { "id": "Input ID", "status": 0, "test": 0, "cmds": [] }
           },
           "temp": 0,
           "temp": 12205
         }
      }
