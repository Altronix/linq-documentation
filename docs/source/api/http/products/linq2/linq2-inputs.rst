Inputs
======

.. http:get:: /ATX/hardware/inputs/(string:index)

   Returns the state of the linq2 input at the respective index

   **Example request**:

   .. sourcecode:: http

      GET /ATX/hardware/inputs/input0 HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "input0": {
           "id": "Input ID",
           "status": 0,
           "test": 0,
           "cmds": [{
             "url": "/ATX/hardware/eflows",
             "data": {
               "mask": "px"
             }
           }]
         }
      }

   :>json string id: The name of the input resource. If an input event is triggered and the alert per this input is not masked, an alert will be recorded and the alerts 'what' property will contain this ID field
   :>json number status: The current state of the input. (1 - active, 0 - in-active).  Note that the input triggers on the LinQ2 module are active low
   :>json object cmds:  An array of commands to execute should the input go active. These commands can be any API command as supported by the device API. (Linq Common API + LinQ2 Product API)
   :>json string url: When the commands array is executed by the input trigger, the URL is the resource location per the API.
   :>json object data: When the commands array is executed by the input trigger, the data property contains POST data included with the URL.
