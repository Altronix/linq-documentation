Relays
======

.. http:get:: /ATX/hardware/relays/(string:index)

   Returns the state of the linq2 relays at the respective index

   **Example request**:

   .. sourcecode:: http

      GET /ATX/hardware/relays/relay0 HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "relay1": {
             "id": "Relay ID", 
             "status": 0, 
             "pulse": -15 
         }
      }

   :>json string id: The name of the relay resource. If an relay event is triggered and the alert per this relay is not masked, an alert will be recorded and the alerts 'what' property will contain this ID field
   :>json number status: The current state of the relay. (1 - active, 0 - in-active).  
   :>json number pulse: number of seconds to pulse the power supply, should a pulse command be executed. (<0 pulse off, >0 pulse on)

.. http:post:: /ATX/hardware/relay/(string:index)/status

   Change the state of the Relay

   **Example request**:

   .. sourcecode:: http

      POST /ATX/hardware/relay/relay0/status HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript
      Content-Type: application/json

      {
         "status":0
      }

   :<json boolean status: If the supplied property is a number, the relay will either turn on (status=1), or turn off (status=0)
   :<json string status: If the supplied property is a string, the relay will either Pulse (status='p') or toggle (status='t'), Where a "pulse" command will either pulse on for x seconds, or pulse off for x seconds, and where seconds is determined by the "pulse" property of the same relay object.

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "error":200
      }
