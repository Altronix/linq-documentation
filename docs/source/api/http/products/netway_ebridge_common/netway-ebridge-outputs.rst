PoE Output channels
===================

.. http:get:: /ATX/hardware/chs/(string:index)

   Returns the state of the PoE Channel at the respective index.

   **Example request**:

   .. sourcecode:: http

      GET /ATX/hardware/chs/ch2 HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
          "_sig": "",
          "ch2": {
              "data": {
                  "v": 185,
                  "i": 0,
                  "class": 0,
                  "state": 0,
                  "mode": 1,
                  "port_en": 1,
                  "port_num": 2,
                  "port_name": "Port ID 3",
                  "pcycl_dur": 5000
              }
          }
      }

   :>json integer v: PoE Channel voltage value in millivolts.
   :>json integer i: PoE Channel current value in milliamps.
   :>json integer class: PoE Channel Power Level Class.
   :>json integer state: A boolean value to indicate if the PoE Channel is active or no-active (1:active, 0:no-active). The PoE is considered 'active' when is delivering power to a PD (PoE Powered Device). 'no-active' means no power is being delivered.
   :>json integer mode: Read only boolean value to indicate if the PoE Channel status (1-enable, 0-disable).
   :>json integer port_en: A Read/Write boolean value to enable or disable the PoE Channel (1-enable, 0-disable).
   :>json integer port_num: PoE Channel Index.
   :>json string port_name: The name of the resource (IE a PoE Channel can be named and are tagged with this name should an alert occur from this resource).
   :>json integer pcycl_dur: number of seconds to pulse the PoE Channel Output, should a pulse command be executed. (<0 pulse off, >0 pulse on)

.. http:post:: /ATX/hardware/chs/(string:index)/data

   Toggle the state of the PoE Channel

   **Example request**:

   .. sourcecode:: http

      POST /ATX/hardware/chs/ch2/data HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript
      Content-Type: application/json

      {
         "port_en":"t"
      }

   :<json string port_en: If the supplied property is a string "t" , the PoE Channel will toggle between the "on/off" states.

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "_sig": "",
         "error": 0
      }

.. http:post:: /ATX/hardware/chs/mask

   Pulse PoE Channel Power Output.

   **Example request**:

   .. sourcecode:: http

      POST /ATX/hardware/chs/mask HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript
      Content-Type: application/json

      {
         "mask":"xpxx"
      }

   :<json string mask: This property follows the pattern "aaaa" with characters from left to right meaning ch1 to ch4. An 'a' = "p" indicates to pulse that specific channel and 'a'="x" indicates no action on the specific port. E.g to pulse PoE Channel 2 the corresponding string will be equal to "xpxx".

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
         "_sig": "",
         "error": 0
      }