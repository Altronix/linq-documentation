Device IO summary
=================

.. http:get:: /ATX/hardware

   Returns a summary of the IO state of all PoE (power over ethernet) ports. For detailed information on JSON object properties, refer to the more explicit non-summary api, (IE: /ATX/hardware/chs/ch0)

   **Example request**:

   .. sourcecode:: http

      GET /ATX/hardware HTTP/1.0
      Host: 192.168.168.168
      Accept: application/json, text/javascript

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json

      {
       "_sig": "",
       "board": {
           "about": {
               "version": "2.00.555",
               "type": "NETWAY4EWP"
           },
           "chs": {
               "mask": "",
               "ch0": {
                   "data": {
                       "v": 185,
                       "i": 0,
                       "class": 0,
                       "state": 0,
                       "mode": 1,
                       "port_en": 1,
                       "port_num": 0,
                       "port_name": "Port ID 1",
                       "pcycl_dur": 5000
                   }
               },
               "ch1": {
                   "data": {
                       "v": 0,
                       "i": 0,
                       "class": 0,
                       "state": 0,
                       "mode": 1,
                       "port_en": 1,
                       "port_num": 1,
                       "port_name": "Port ID 2",
                       "pcycl_dur": 5000
                   }
               },
               "ch2": {
                   "data": {
                       "v": 0,
                       "i": 0,
                       "class": 0,
                       "state": 0,
                       "mode": 1,
                       "port_en": 1,
                       "port_num": 2,
                       "port_name": "Port ID 3",
                       "pcycl_dur": 5000
                   }
               },
               "ch3": {
                   "data": {
                       "v": 0,
                       "i": 0,
                       "class": 0,
                       "state": 0,
                       "mode": 1,
                       "port_en": 1,
                       "port_num": 3,
                       "port_name": "Port ID 4",
                       "pcycl_dur": 5000
                   }
               }
           },
           "status": {
               "temp": 36,
               "alert_cnt": 2
           },
           "cfg": {
               "id": 0,
               "temp_high": 50
           },
           "wdt": {
               "wdt_cnt": 2
           },
           "reboot": {
               "cnt": 2
           },
           "exe": {
               "save": "application"
           },
           "unix": 1475285335
       }
      }
