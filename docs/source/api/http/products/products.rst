Products
========

The following documentation outlines the Altronix API specific to the Altronix LinQ product.  Product API's include all of the LinQ HTTP API (common) resources plus the additional product specific API's.

LinQ2
-----

.. toctree::
   :maxdepth: 4

   linq2/linq2-alerts.rst
   linq2/linq2-hardware.rst
   linq2/linq2-eflows.rst
   linq2/linq2-inputs.rst
   linq2/linq2-relays.rst

LinQ8
-----

.. toctree::
   :maxdepth: 4

   linq8/linq8.rst


Netway / Ebridge
----------------

.. toctree::
   :maxdepth: 4

   netway_ebridge_common/netway-ebridge-hardware.rst
   netway_ebridge_common/netway-ebridge-outputs.rst
