Tips and Tricks
===============

Extend Classes
--------------

If you want to append to an SDK class, you can create a class to wrap around the SDK class.
This class can have additional functions or Member Fields.
This can be useful for handling UI.
For example a member field and function could be added to handle hiding or showing alerts.
You can name these extenstions anything but we recommend something like 'ExtendedAlert' or 'ExtendedDevice' to more easily identify them as extensions of other classes.

Example
^^^^^^^

.. code-block:: csharp

  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;

  namespace CSharpWPFExample
  {
      public class ExtendedAlert
      {
          public string SiteId { get; set; }
          public string name { get; set; }
          public string type { get; set; }
          public string time { get; set; }
          public string user { get; set; }
          public string message { get; set; }
          public string detailedViewVisibility { get; set; }
          public string visibility { get; set; }

          public ExtendedAlert() { }

          public ExtendedAlert(String SiteId, String name, String type, String time, String user, String message)
          {
              this.SiteId = SiteId;
              this.name = name;
              this.type = type;
              this.time = time;
              this.user = user;
              this.message = message;
              this.detailedViewVisibility = "Collapsed";
              this.visibility = "Visible";
          }

          // Additional function for hidding a UI popup
          public void ToggleDetailedViewVisibility()
          {
              if (this.detailedViewVisibility == "Collapsed")
              {
                  this.detailedViewVisibility = "Visible";
              }
              else
              {
                  this.detailedViewVisibility = "Collapsed";
              }
          }

          // Additional function for toggle visibility of alert
          public void ToggleVisibility()
          {
              if (this.visibility == "Collapsed")
              {
                  this.visibility = "Visible";
              }
              else
              {
                  this.visibility = "Collapsed";
              }

              this.detailedViewVisibility = "Collapsed";
          }
      }
  }
