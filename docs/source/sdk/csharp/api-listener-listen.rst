.. _ref-listener-listen:

atx.Listen
==========

.. code-block:: csharp

  void Listen(Device device)

Start a background thread that listens for Alerts from a Device

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  device    Device An instance of Class Device
  ========= ====== ===========

Output
------

None

Examples
--------

.. code-block:: csharp

  // Create instance of Altronix.LINQ.Device
  Device device = new Device("10.10.10.188",  // ip addresss
                              "443",          // port number
                              true,           // TLS/SSL = true
                              "admin",        // username
                              "admin");       // password

  // Create Altronix.LINQ.Listener Class instance
  Listener atx = new Listener();

  // Add listeners here, such as UserError and OnAlerts

  // Start Listening on device
  await atx.Listen(device);
