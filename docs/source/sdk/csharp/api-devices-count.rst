.. _ref-devices-count:

devices.count
=============

.. code-block:: csharp

  int devices.count()

Returns the number of devices that the instance of Devices contains

Params
------

None

Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  int    number of devices that the instance of Devices contains
  ====== ===========

Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  Console.WriteLine(devices.count()); // 0
  devices.addDevice(device);
  Console.WriteLine(devices.count()); // 1
