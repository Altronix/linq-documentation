.. _ref-devicestorage-activatesetupstate:

deviceStorage.activateSetupState
================================

.. code-block:: csharp

  void deviceStorage.activateSetupState()

Resets setupState to 'true'. Calling DeviceStorage.getDevices() will read devices from an IsolatedStorageFile: TrackedDevices.txt instead of from Class Devices and reset setupState = false

Params
------

None

Output
------

None

Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();

  deviceStorage.activateSetupState();
  //Calling DeviceStorage.getDevices() will read devices from an IsolatedStorageFile
