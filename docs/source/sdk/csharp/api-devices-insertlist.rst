.. _ref-devices-insertlist:

devices.insertList
==================

.. code-block:: csharp

  void devices.insertList(List<Device> devices)

Override the current list of devices with a new list of devices

Params
------

.. table::
  :align: left

  ========= ============ ===========
  parameter type         description
  ========= ============ ===========
  devices   List<Device> A list of LinQ Device class instances
  ========= ============ ===========

Output
------

None

Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  devices.addDevice(device);
  Console.WriteLine(devices.count()); // 1

  List<Device> newDeviceList = {
    new Device("10.10.10.10", "81", false, "username", "password"),
    new Device("10.10.10.10", "82", false, "username", "password"),
    new Device("10.10.10.10", "83", false, "username", "password"),
    new Device("10.10.10.10", "84", false, "username", "password")
  }

  devices.insertList(newDeviceList);
  Console.WriteLine(devices.count()); // 4
