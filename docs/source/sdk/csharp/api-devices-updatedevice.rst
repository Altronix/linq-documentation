.. _ref-devices-updatedevice:

devices.updateDevice
====================

.. code-block:: csharp

  Boolean devices.updateDevice(String deviceId, Device device)

Update device in list by authority id

Params
------

.. table::
  :align: left

  ========= ======== ===========
  parameter type     description
  ========= ======== ===========
  deviceId  String   Id string used to look up a specific device
  device    Device   Device instance to replace instance of deviceId
  ========= ======== ===========

Output
------

.. table::
  :align: left

  ======= ===========
  type    description
  ======= ===========
  Boolean Returns true if device was successfully updated, otherwise false including if device is not in list
  ======= ===========

Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  Console.WriteLine(device.ToString());

  devices.addDevice(device);
  devices.updateDevice(device.authority, new Device("10.10.10.10", "80", false, "username2", "password2"));

  Device updatedDevice = devices.getDevice(device.authority);

  Console.WriteLine(updatedDevice.ToString()); 
