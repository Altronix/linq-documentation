.. _ref-devices-removedevice:

devices.removeDevice
====================

.. code-block:: csharp

  Boolean devices.removeDevice(String deviceId)

Remove a device from the list of devices

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  deviceId  String Id string used to look up a specific device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ======= ===========
  type    description
  ======= ===========
  Boolean Returns true if device was successfully deleted, otherwise false
  ======= ===========

Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  devices.addDevice(device);
  Console.WriteLine(devices.count()); // 1
  devices.deleteDevice(device.authority);
  Console.WriteLine(devices.count()); // 0
