.. _ref-devicestorage-updatedevice:

deviceStorage.updateDevice
==========================

.. code-block:: csharp

  Boolean deviceStorage.updateDevice(String deviceId, Device updateThisDevice)

Update device in list by authority id

Params
------

.. table::
  :align: left

  ========= ======== ===========
  parameter type     description
  ========= ======== ===========
  deviceId  deviceId Id string used to look up a specific device
  ========= ======== ===========

Output
------

.. table::
  :align: left

  ======= ===========
  type    description
  ======= ===========
  Boolean Returns true if device was successfully updated, otherwise false including if device is not in list
  ======= ===========

Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  Console.WriteLine(device.ToString());

  deviceStorage.addDevice(device);
  deviceStorage.updateDevice(device.authority, new Device("10.10.10.10", "80", false, "username2", "password2"));

  Device updatedDevice = devices.getDevice(device.authority);

  Console.WriteLine(updatedDevice.ToString());
