.. _ref-device-setauthority:

device.setAuthority
===================

.. code-block:: csharp

  void device.setAuthority(String authority)

Assigns a string value to device.authority

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  authority String A string in the format <ip>:<port> used to identify a device
  ========= ====== ===========

Output
------

None

Examples
--------

.. code-block:: csharp

  string deviceId = "10.10.10.10:80"
  Device device = new Device();
  device.setAuthority(deviceId);

  Console.WriteLine(device.authority);
