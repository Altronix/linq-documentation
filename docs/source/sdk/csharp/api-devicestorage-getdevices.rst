.. _ref-devicestorage-getdevices:

deviceStorage.getDevices
========================

.. code-block:: csharp

  List<Device> deviceStorage.getDevices()

Returns a list of all devices contained in instance

Params
------

None

Output
------

.. table::
  :align: left

  ============ ===========
  type         description
  ============ ===========
  List<Device> A list of devices of class Device
  ============ ===========

Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  deviceStorage.addDevice(device);

  List<Device> listOfDevices = deviceStorage.getDevices();
