.. _ref-devicestorage-deletedevice:

deviceStorage.deleteDevice
==========================

.. code-block:: csharp

  Boolean deviceStorage.deleteDevice(String deviceId)

Remove a device from the list of devices

Params
------

.. table::
  :align: left

  ========= ======== ===========
  parameter type     description
  ========= ======== ===========
  deviceId  deviceId Id string used to look up a specific device
  ========= ======== ===========

Output
------

.. table::
  :align: left

  ======= ===========
  type    description
  ======= ===========
  Boolean Returns true if device was successfully deleted, otherwise false
  ======= ===========

Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  deviceStorage.deleteDevice(device.authority);
