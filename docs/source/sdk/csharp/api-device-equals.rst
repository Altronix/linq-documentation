.. _ref-device-equals:

device.Equals
=============

.. code-block:: csharp

  bool device.Equals(Device device)

Compares the current device instance with another device by authority (<IP>:<port>) and returns true if the authority of the two instances are equal and false if not

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  device    Device An instance of the LinQ class Device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  bool   true if authority is a String and its value is the same as this instance; otherwise, false. If authority is null, the method returns false
  ====== ===========

Examples
--------

.. code-block:: csharp

  // device_one.authority = "10.10.10.10:80"
  // device_two.authority = "10.10.10.10:81"
  Device device_one = new Device("10.10.10.10", "80", false, "username", "password");
  Device device_two = new Device("10.10.10.10", "81", false, "username", "password");

  int equalsResult = device_one.Equals(device_two);

  if (equalsResult) {
    Console.WriteLine("device_one equals device_two")
  } else {
    Console.WriteLine("device_one does not equal device_two")
  }
