.. _ref-alert:

Alert
=====

Class that represents a device Alert

Constructor(s)
--------------

.. table::
  :align: left

  ============== =============
  constructor(s) description
  ============== =============
  Alert()        Creates an instance of Alert
  ============== =============

Member Fields
-------------

.. table::
  :align: left

  ======== ======
  property type
  ======== ======
  user     string
  date     string
  message  string
  ip       string
  port     string
  siteid   string
  ======== ======
