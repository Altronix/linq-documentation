.. _ref-devices-getdevices:

devices.getDevices
==================

.. code-block:: csharp

  List<Device> devices.getDevices()

Returns a list of all devices contained in instance

Params
------

None

Output
------

.. table::
  :align: left

  ============ ===========
  type         description
  ============ ===========
  List<Device> A list of devices of class Device
  ============ ===========


Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  devices.addDevice(device);

  List<Device> listOfDevices = devices.getDevices();
