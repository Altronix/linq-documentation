Overview
========

Dependencies
------------

  * Built for .NET 4.5.2 and up.
  * Newtonsoft.Json

Installation
------------

Once a project has been setup, to add C# SDK please follow the steps below:

  1.  In the Solution Explorer, right-click the References section found under [Project name] -> References

      .. image:: setup_1.jpg
        :align: center

  2.  Click ‘Add Reference...’

      .. image:: setup_2.jpg
        :align: center

  3.  In the Reference Manager go to ‘Browse’ subsection

      .. image:: setup_3.jpg
        :align: center

  4.  Click the ‘Browse...’button

      .. image:: setup_4.jpg
        :align: center

  5.  Locate the AltronixAlerts.dll file on your local computer

      .. image:: setup_5.jpg
        :align: center

  6.  Once located, select the file and then click ‘Add’

      .. image:: setup_6.jpg
        :align: center

  7.  You will return to the Reference Manager where you can check to confirm that the library has been added and click ‘OK’

      .. image:: setup_7.jpg
        :align: center

  8.  Under References you should now see ‘AltronixAlerts.dll’ listed

      .. image:: setup_8.jpg
        :align: center

  9.  To use the library add the following line to the top of a file: ‘using Altronix.LINQ;’

      .. image:: setup_9.jpg
        :align: center

Import
------

.. code-block:: csharp

  using Altronix.LINQ;

Setup for listening to devices
------------------------------


.. code-block:: csharp

  // User Callback that triggers OnAlerts events
  public void UserCallback(List<Alert> list)
  {
      list.ForEach(alert =>
      {
          Console.WriteLine(
            "New Alert:\n"
            + "Site ID: " + alert.siteid + "\n"
            + "Message: " + alert.message + "\n"
            + "Date: " + alert.date + "\n"
            + "Address: " + alert.ip + ":" + alert.port + "\n"
            );
      });
  }

  // User Callback that triggers OnError events
  public void UserErrorCallback(Device device)
  {
      switch (device.error)
      {
          case Error.SUCCESS:
              Console.WriteLine("Success connecting to " + device.ip + ":" + device.port + "\n");
              break;
          case Error.CONNECTING:
              Console.WriteLine("Error connecting to " + device.ip + ":" + device.port + "\n");
              break;
          case Error.AUTHENTICATE:
              Console.WriteLine("Error authenticating on " + device.ip + ":" + device.port + "\n");
              break;
          case Error.FORBIDDEN:
              Console.WriteLine("Error access forbidden on " + device.ip + ":" + device.port + "\n");
              break;
      }
  }

  // Async function creates a listener for one device
  public async Task ListenToNewDevice(Device device)
  {
      // Create Listener Class instance
      Listener atx = new Listener();

      // Set user's callback for OnAlerts Event
      atx.OnAlerts(UserCallback);

      // Set user's callback for OnError Event
      atx.OnError(UserErrorCallback);

      // Start Listening on device
      atx.Listen(device);
  }

  // Async function creates a listener for a list of devices
  public async Task ListenToAllDevices(List<Device> devices)
  {
      // Create Listener Class instance
      Listener atx = new Listener();

      // Set user's callback for OnAlerts Event
      atx.OnAlerts(UserCallback);

      // Set user's callback for OnError Event
      atx.OnError(UserErrorCallback);

      // Start Listening to device
      atx.ListenAll(devices);
  }

Listen for events on one device
-------------------------------

.. code-block:: csharp

  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  await ListenToNewDevice(device); // Creates Listener for one new device

Listen for events on list of devices
------------------------------------

.. code-block:: csharp

  List<Device> devices = DeviceStorage.getDevices(); // Get or create list of devices you want to listen to

  await ListenToAllDevices(devices); // Creates Listeners for all devices in list

Example
----------------

.. code-block:: csharp

  using System;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.Data;
  using System.Drawing;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Windows.Forms;
  using Altronix.LINQ;

  namespace AlertsNugetTest
  {
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void ListenToSingleDevice_Click(object sender, EventArgs e)
        {
            // Create instance of Altronix.LINQ.Device
            Device device = new Device("10.10.10.188",  // ip addresss
                                        "443",          // port number
                                        true,           // TLS/SSL = true
                                        "admin",        // username
                                        "admin");       // password

            // Create Altronix.LINQ.Listener Class instance
            Listener atx = new Listener();

            // Set user's callback for OnAlerts Event
            atx.OnAlerts(UserCallback);

            // Set user's callback for OnError Event
            atx.OnError(UserErrorCallback);

            // Start Listening on device
            await atx.Listen(device);

        }

        // User Callback that triggers OnAlerts events
        public void UserCallback(List<Alert> list)
        {
            list.ForEach(alert => {
                var str = "[ " + alert.date + " ]  [ " + alert.siteid + alert.ip + ":" + alert.port + " ] ===> " +
                            alert.message + "\n";
                // Print to console
                Console.WriteLine(str);
            });
        }

        public void UserErrorCallback(Device device)
        {
            switch (device.error)
            {
                case Error.CONNECTING:
                    /* Handle this error here */
                    break;
                case Error.AUTHENTICATE:
                    /* Handle this error here */
                    break;
                case Error.FORBIDDEN:
                    /* Handle this error here */
                    break;
            }
        }

        private void ListenToSeveralDevices2_Click(object sender, EventArgs e)
        {
            // Create List<Altronix.LINQ.Device> of devices
            List<Device> devices = new List<Device>
            {
                new Device(){ ip="10.10.10.183",port="80",ssl=false,username="admin",password="admin"},
                new Device(){ ip="10.10.10.185",port="443",ssl=true,username="admin",password="admin"},
                new Device(){ ip="10.10.10.182",port="443",ssl=true,username="admin",password="admin"},
                new Device(){ ip="10.10.10.186",port="443",ssl=true,username="admin",password="123"},
                new Device(){ ip="10.10.10.188",port="443",ssl=true,username="admin",password="admin"}
            };

            // Create Altronix.LINQ.Listener Class instance
            Listener atx = new Listener();

            // Set user's callback for OnAlerts Event
            atx.OnAlerts(UserCallback);

            // Set user's callback for OnError Event
            atx.OnError(UserErrorCallback);

            // Start Listening on list of devices
            atx.ListenAll(devices);

        }
    }

  }
