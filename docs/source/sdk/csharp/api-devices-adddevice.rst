.. _ref-devices-adddevice:

devices.addDevice
=================

.. code-block:: csharp

  Boolean devices.addDevice(Device device)

Adds a new device to devices. Returns true if device was successfully added

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  device    Device An instance of the LinQ class Device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ======= ===========
  type    description
  ======= ===========
  Boolean Returns true if device was successfully added, otherwise false
  ======= ===========

Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  Console.WriteLine(devices.count()); // 0
  devices.addDevice(device);
  Console.WriteLine(devices.count()); // 1
