.. _ref-devicestorage-savedevices:

deviceStorage.saveDevices
=========================

.. code-block:: csharp

  void deviceStorage.saveDevices()

Stores devices in an IsolatedStorageFile: TrackedDevices.txt

Params
------

None

Output
------

None

Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  deviceStorage.addDevice(device);

  deviceStorage.saveDevices();
