.. _ref-devicestorage:

DeviceStorage
=============

Class that represents local storage of connected devices

Constructor(s)
--------------

.. table::
  :align: left

  =============== =============
  constructor(s)  description
  =============== =============
  DeviceStorage() Creates an instance DeviceStorage
  =============== =============

Functions
---------

.. table::
  :align: left

  =========================================== =================
  property                                    description
  =========================================== =================
  :ref:`ref-devicestorage-activatesetupstate`
  :ref:`ref-devicestorage-getdeviceid`
  :ref:`ref-devicestorage-savedevices`
  :ref:`ref-devicestorage-getdevices`
  :ref:`ref-devicestorage-adddevice`
  :ref:`ref-devicestorage-getdevice`
  :ref:`ref-devicestorage-deletedevice`
  :ref:`ref-devicestorage-updatedevice`
  =========================================== =================

.. toctree::
  :hidden:
  :maxdepth: 1

  api-devicestorage-activatesetupstate
  api-devicestorage-getdeviceid
  api-devicestorage-savedevices
  api-devicestorage-getdevices
  api-devicestorage-adddevice
  api-devicestorage-getdevice
  api-devicestorage-deletedevice
  api-devicestorage-updatedevice
