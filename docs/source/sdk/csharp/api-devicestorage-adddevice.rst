.. _ref-devicestorage-adddevice:

deviceStorage.addDevice
=======================

.. code-block:: csharp

  Boolean deviceStorage.addDevice(Device device)

  Adds a new device to deviceStorage. Returns true if device was successfully added


Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  device    Device An instance of the LinQ class Device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ======= ===========
  type    description
  ======= ===========
  Boolean Returns true if device was successfully added, otherwise false
  ======= ===========

Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  deviceStorage.addDevice(device);
