.. _ref-device-tostring:

device.ToString
===============

Creates status string of the device in the format "Authority: <authority> Username: <username> Error: <error_code>";

.. code-block:: csharp

  string device.ToString()

Params
------

None

Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  string String formatted status of device "Authority: <authority> Username: <username> Error: <error_code>"
  ====== ===========


Examples
--------

.. code-block:: csharp

  Device device = new Device("10.10.10.10", "80", false, "username", "password");
  Console.WriteLine(device.ToString()); // logs "Authority: 10.10.10.10:80 Username: username Error: 0"
