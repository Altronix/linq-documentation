.. _ref-listener:

Listener
========

Constructor(s)
--------------

.. table::
  :align: left

  ============== =============
  constructor(s) description
  ============== =============
  Listener()     Creates an instance of the Listener class
  ============== =============

Member Fields
-------------

.. table::
  :align: left

  ===================================== ======
  property                              type
  ===================================== ======
  AlertsCallback(List<Alert> alertList) Callback function
  ErrorCallback(Device device);         Callback function
  ===================================== ======

Functions
------------

.. table::
  :align: left

  ================================== =====================
  property                           description
  ================================== =====================
  :ref:`ref-listener-listen`         Start a background thread that listens for Alerts from a Device
  :ref:`ref-listener-listenall`      Start a background thread that listens for Alerts for each Device in AltronixDeviceList
  :ref:`ref-listener-onalerts`       Sets the user callback to trigger on Alerts events
  :ref:`ref-listener-onerror`        Sets the user callback to trigger on Errors events
  ================================== =====================

.. toctree::
  :hidden:
  :maxdepth: 1

  api-listener-listen.rst
  api-listener-listenall.rst
  api-listener-onalerts.rst
  api-listener-onerror.rst
