.. _ref-devices-getdevice:

devices.getDevice
=================

.. code-block:: csharp

  Device devices.getDevice(String deviceId)

If device is in devices, an instance of it will be returned

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  deviceId  String Id string used to look up a specific device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  Device If device by same authority is in the list, its Device instance will be returned, else null
  ====== ===========

Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  Device newDeviceInstance = devices.getDevice(device.authority);
  Console.WriteLine(newDeviceInstance.ToString());
