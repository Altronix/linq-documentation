.. _ref-device:

Device
======

Class that represents an Altronix LINQ device

Constructor(s)
--------------

.. table::
  :align: left

  =========================================================================================== =============
  constructor(s)                                                                              description
  =========================================================================================== =============
  Device()                                                                                    Creates an empty instance of Device
  Device(string Nickname, string Ip, string Port, bool Ssl, string Username, string Password) Creates instance of Device with initial member fields
  Device(string Ip, string Port, bool Ssl, string Username, string Password)                  Creates instance of Device with initial member fields but no username
  =========================================================================================== =============

Member Fields
-------------

.. table::
  :align: left

  ========= ======
  property  type
  ========= ======
  ip        string
  port      string
  ssl       bool
  username  string
  password  string
  nickname  string
  error     uint
  authority string
  ========= ======

Functions
------------

.. table::
  :align: left

  ============================== =====================
  property                       description
  ============================== =====================
  :ref:`ref-device-setauthority` Sets the authority class variable
  :ref:`ref-device-tostring`     returns error in string form
  :ref:`ref-device-gethashcode`
  :ref:`ref-device-equals`       compares two devices and returns true if they are equal
  :ref:`ref-device-compareto`    compares authority of device against another device
  ============================== =====================

.. toctree::
  :hidden:
  :maxdepth: 1

  api-device-setauthority.rst
  api-device-tostring.rst
  api-device-gethashcode.rst
  api-device-equals.rst
  api-device-compareto.rst
