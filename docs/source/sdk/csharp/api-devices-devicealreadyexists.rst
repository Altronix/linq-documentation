.. _ref-devices-devicealreadyexists:

devices.deviceAlreadyExists
===========================

.. code-block:: csharp

  Boolean devices.deviceAlreadyExists(String deviceId)

Returns true if a device with the same authority string already exists in the list of devices

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  deviceId  String Id string used to look up a specific device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ======= ===========
  type    description
  ======= ===========
  Boolean Returns true if a device with the same authority string already exists in the list of devices
  ======= ===========

Examples
--------

.. code-block:: csharp

  Devices devices = new Devices();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  if (devices.deviceAlreadyExists(device.authority)) {
    Console.WriteLine("device already in list")
  } else {
    Console.WriteLine("device not in list") // This will print
  }

  devices.addDevice(device);

  if (devices.deviceAlreadyExists(device.authority)) {
    Console.WriteLine("device already in list") // This will print
  } else {
    Console.WriteLine("device not in list")
  }
