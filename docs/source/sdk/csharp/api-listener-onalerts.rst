.. _ref-listener-onalerts:

atx.OnAlerts
============

.. code-block:: csharp

  void OnAlerts(Listener.AlertsCallback callback)

Sets the user callback to trigger on Alerts events

Params
------

.. table::
  :align: left

  ========= ======================= ===========
  parameter type                    description
  ========= ======================= ===========
  callback  Listener.AlertsCallback A method with 'void AlertsCallback(List<Alert> alertList)' signature
  ========= ======================= ===========

Output
------

None

Examples
--------

.. code-block:: csharp

  // Create Listener Class instance
  Listener atx = new Listener();

  // Set user's callback for OnAlerts Event
  atx.OnAlerts(UserCallback);

  // Sample User Callback that triggers OnAlerts events
  public void UserCallback(List<Alert> list)
  {
      list.ForEach(alert => {
          var str = "[ " + alert.date + " ]  [ " + alert.siteid + alert.ip + ":" + alert.port + " ] ===> " +
                      alert.message + "\n";
          // Print to console
          Console.WriteLine(str);
      });
  }
