.. _ref-error:

Error
=====

A class of constants used to set the current state of a device

Error Codes
-----------

.. table::
  :align: left

  ============ ===== ===========
  name         value description
  ============ ===== ===========
  SUCCESS      0     Successfully connected to device
  CONNECTING   1     Connecting to device
  AUTHENTICATE 2     Authenticating connection
  FORBIDDEN    3     Connection forbidden/rejected
  ============ ===== ===========

Example
-------

.. code-block:: csharp

  public void UserErrorCallback(Device device)
     {
         switch (device.error)
         {
             case Error.SUCCESS:
                 Console.WriteLine("Successfully connected")
                 break;
             case Error.CONNECTING:
                 Console.WriteLine("Error connecting to " + device.ip + ":" + device.port + "\n");
                 break;
             case Error.AUTHENTICATE:
                 Console.WriteLine("Error authenticating on " + device.ip + ":" + device.port + "\n");
                 break;
             case Error.FORBIDDEN:
                 Console.WriteLine("Error access forbidden on " + device.ip + ":" + device.port + "\n");
                 break;
         }
     }
