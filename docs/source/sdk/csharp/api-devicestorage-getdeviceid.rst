.. _ref-devicestorage-getdeviceid:

deviceStorage.getDeviceId
=========================

.. code-block:: csharp

  String deviceStorage.getDeviceId(Device device)

Returns the id of the device

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  device    Device An instance of the LinQ class Device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  String String ID of device (its authority)
  ====== ===========


Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  String deviceId = deviceStorage.getDeviceId(device);
  Console.WriteLine(deviceId);
