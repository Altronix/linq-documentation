.. _ref-listener-listenall:

atx.ListenAll
=============

.. code-block:: csharp

  void ListenAll(List<Device> device_list)

Start a background thread that listens for Alerts for each Device in AltronixDeviceList

Params
------

.. table::
  :align: left

  =========== ============ ===========
  parameter   type         description
  =========== ============ ===========
  device_list List<Device> An instance of List<Device> device_list
  =========== ============ ===========

Output
------

None

Examples
--------

.. code-block:: csharp

  // Create List<Altronix.LINQ.Device> of devices
  List<Device> devices = new List<Device>
  {
      new Device(){ ip="10.10.10.183",port="80",ssl=false,username="admin",password="admin"},
      new Device(){ ip="10.10.10.185",port="443",ssl=true,username="admin",password="admin"},
      new Device(){ ip="10.10.10.182",port="443",ssl=true,username="admin",password="admin"},
      new Device(){ ip="10.10.10.186",port="443",ssl=true,username="admin",password="123"},
      new Device(){ ip="10.10.10.188",port="443",ssl=true,username="admin",password="admin"}
  };

  // Create Altronix.LINQ.Listener Class instance
  Listener atx = new Listener();

  // Add listeners here, such as UserError and OnAlerts

  // Start Listening on list of devices
  atx.ListenAll(devices);
