.. _ref-listener-onerror:

atx.OnError
===========

.. code-block:: csharp

  void OnError(Listener.ErrorCallback callback_error)

Sets the user callback to trigger on Errors events

Params
------

.. table::
  :align: left

  ========= ======================= ===========
  parameter type                    description
  ========= ======================= ===========
  callback  Listener.ErrorCallback  A method with 'void ErrorCallback(Device device)' signature
  ========= ======================= ===========

Output
------
None

Examples
--------

.. code-block:: csharp

  // Create Altronix.LINQ.Listener Class instance
  Listener atx = new Listener();

  // Set user's callback for OnError Event
  atx.OnError(UserErrorCallback);

  // Sample User Callback that triggers OnError events
  public void UserErrorCallback(Device device)
  {
      switch (device.error)
      {
          case Error.CONNECTING:
              /* Handle this error here */
              break;
          case Error.AUTHENTICATE:
              /* Handle this error here */
              break;
          case Error.FORBIDDEN:
              /* Handle this error here */
              break;
      }
  }
