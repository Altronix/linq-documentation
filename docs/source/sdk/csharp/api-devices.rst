.. _ref-devices:

Devices
=======

Class that represents a list of Altronix LINQ devices

Constructor(s)
--------------

.. table::
  :align: left

  ============================= =============
  constructor(s)                description
  ============================= =============
  Devices()                     Creates an empty instance with an empty list of devices
  Devices(List<Device> devices) Creates an instance with an initial list of devices
  ============================= =============

Member Fields
-------------

.. table::
  :align: left

  ======== ============
  property type
  ======== ============
  devices  List<Device>
  ======== ============

Functions
---------

.. table::
  :align: left

  ====================================== ===========
  property                               description
  ====================================== ===========
  :ref:`ref-devices-insertlist`
  :ref:`ref-devices-adddevice`
  :ref:`ref-devices-removedevice`
  :ref:`ref-devices-getdevice`
  :ref:`ref-devices-getdevices`
  :ref:`ref-devices-updatedevice`
  :ref:`ref-devices-devicealreadyexists`
  :ref:`ref-devices-count`
  ====================================== ===========

.. toctree::
  :hidden:
  :maxdepth: 1

  api-devices-insertlist
  api-devices-adddevice
  api-devices-removedevice
  api-devices-getdevice
  api-devices-getdevices
  api-devices-updatedevice
  api-devices-devicealreadyexists
  api-devices-count
