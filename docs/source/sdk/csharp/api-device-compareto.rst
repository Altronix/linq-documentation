.. _ref-device-compareto:

device.CompareTo
================

.. code-block:: csharp

  int device.CompareTo(Device device)

Compares the current device instance with another device by authority (<IP>:<port>) and returns an integer that indicates
whether the current instance precedes, follows, or occurs in the same position in the sort order as the other device.

Params
------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  device    Device An instance of the LinQ class Device
  ========= ====== ===========

Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  int    A value that indicates the relative order of the devices being compared. if < 0, this instance precedes the device, if = 0, the instance is at the same position as the device, if > 0 instance follows the device in order
  ====== ===========

Examples
--------

.. code-block:: csharp

  // device_one.authority = "10.10.10.10:80"
  // device_two.authority = "10.10.10.10:81"
  Device device_one = new Device("10.10.10.10", "80", false, "username", "password");
  Device device_two = new Device("10.10.10.10", "81", false, "username", "password");

  int compareResult = device_one.CompareTo(device_two);

  if (compareResult < 0) {
    Console.WriteLine("device_one precedes device_two")
  } else if (compareResult == 0) {
    Console.WriteLine("device_one is at same position as device_two")
  } else {
    Console.WriteLine("device_one follows device_two")
  }
