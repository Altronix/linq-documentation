.. _ref-device-gethashcode:

device.GetHashCode
==================

.. code-block:: csharp

  int device.GetHashCode()

Serves as the device hash function.

Params
------

None


Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  int32  A hash code for the current object
  ====== ===========


Examples
--------

.. code-block:: csharp

  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  int deviceHashCode = device.GetHashCode();

  Console.WriteLine(deviceHashCode)
