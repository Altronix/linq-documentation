.. _ref-devicestorage-getdevice:

deviceStorage.getDevice
=======================

.. code-block:: csharp

  Device deviceStorage.getDevice(String deviceId)

If device is in devices, an instance of it will be returned

Params
------

.. table::
  :align: left

  ========= ======== ===========
  parameter type     description
  ========= ======== ===========
  deviceId  deviceId Id string used to look up a specific device
  ========= ======== ===========

Output
------

.. table::
  :align: left

  ====== ===========
  type   description
  ====== ===========
  Device If device by same authority is in the list, its Device instance will be returned, else null
  ====== ===========

Examples
--------

.. code-block:: csharp

  DeviceStorage deviceStorage = new DeviceStorage();
  Device device = new Device("10.10.10.10", "80", false, "username", "password");

  Device newDeviceInstance = deviceStorage.getDevice(device.authority);
  Console.WriteLine(newDeviceInstance.ToString());
