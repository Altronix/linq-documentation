.. _ref-linq-timeout:

linq.timeout
============

.. code-block:: javascript

  linq.timeout(ms?: number): Linq

The linq.timeout() method will remove known devices from the list of connected
devices if they have not been seen by the application in x amount of seconds. 

For example: Remove all devices that have not been heard from in over 1 day, and
log the remaining devices.

.. code-block:: javascript

  let devices = linq.timeout(86400).devices();
  console.log(devices);

Parameter
---------

.. table::
  :align: left

  ========= ====== =================
  Parameter type   description
  ========= ====== =================
  ms        number Number of seconds
  ========= ====== =================
