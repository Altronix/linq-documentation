Tips and Tricks
===============

Manage device timeouts
----------------------

When a device connects to linqjs, the device contexts are cached indefinitely. If you are interested to know when a device is not likely connected anymore you can use the "lastSeen" property on the device context.
