.. _ref-device-product:

device.product
==============

.. code-block:: javascript

  device.product(): string;

The device.product() method will return the product string of the device.

Parameters
----------

None.

Valid product strings
---------------------

.. table::
  :align: left

  ========== =======
  string     product
  ========== =======
  "LINQ2"    https://www.altronix.com/products/LINQ2
  "LINQ8PD"  https://www.altronix.com/products/LINQ8PD
  "LINQ8ACM" https://www.altronix.com/products/LINQ8ACM
  ========== =======
