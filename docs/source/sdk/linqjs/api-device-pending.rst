.. _ref-device-pending:

device.pending
==============

.. code-block:: javascript

  device.pending(): number;

The device.pending method will return the number of requests in the queue waiting to be fullfilled by the device.

Parameters
----------

None.

Example
-------

.. code-block:: javascript

  if (device.pending() > 10) {
    // ruh-roh
  }
