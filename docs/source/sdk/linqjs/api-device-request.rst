.. _ref-device-request:

device.request
==============

.. code-block:: javascript

  device.request(path: string): Promise;
  device.request(path: string, data: object): Promise;
  device.request(request: RequestParams, optional_data?: object): Promise;

The device.request method will add a request to the queue and return a promise when the request is fullfilled by the device. The arguments match typically HTTP syntax and the API is the same. For a list of the API, see the HTTP API section of this document.

.. _ref-device-request-params:

RequestParams
-------------

.. table::
  :align: left

  ========= ====== ===========
  parameter type   description
  ========= ====== ===========
  path      string A URL representing the resource for the request
  data?     any    Optional data if this is a post request
  timeout?  number Optional amount of milliseconds to wait for a response (default 8 seconds)
  retry?    number Optional number of times to try request before throwing error
  ========= ====== ===========

.. _ref-device-request-errors:

Supported Errors
----------------

.. table::
  :align: left

  ======= =============
  error   description
  ======= =============
  timeout The device took to long to respond to the request
  cancel  The caller destroyed all pending requests
  device  The device sent an error
  ======= =============

Example GET Request
-------------------

.. code-block:: javascript

  device.request("/ATX/about")
    .then(resp => console.log(resp))
    .catch(error => console.log(error));

Example POST Request
--------------------

.. code-block:: javascript

  device.request("/ATX/network/ip",{ip: "192.168.0.111"})
    .then(resp => console.log(resp))
    .catch(error => console.log(error));


.. note:: The underlying transport protocol being used is ZMTP (+TLS). The API just looks like HTTP
