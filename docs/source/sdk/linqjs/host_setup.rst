Installation
------------

```
npm install --save @altronix/linq
```

Import
------

.. code-block:: javascript

   const linq = require("@altronix/linq").linq;

Listen for events
-----------------

.. code-block:: javascript

   linq.on("new", device => console.log("New device connected! [%s]", device.serial()))
       .on("alert", (device, alert) => console.log("Received alert from device!"))
       .on("error", error => console.log(error));

Start LinQJS
------------

.. code-block:: javascript

  const config = {
    key: key,     // x509 TLS Private Key (optional)
    cert: cert,   // x509 TLS Public Certificate (optional)
    tcps: 99443,  // Secure Port for incoming secure device connections
    tcp: 9980     // Plain text port for incoming non secure device connections
  };
  linq.listen(config).then(() => console.log("LinQ Ready!"))

Example
----------------

app.js

.. code-block:: javascript

   /**
    * Basic app to print alerts to console
    */
   
   let config = require("./config"),
     linq = require("@altronix/linq").linq;
   
   // Setup event callback handlers
   linq
     .on("error", e => console.log(e))
     .on("new", d => console.log("[%s] new device", d.serial()))
     .on("alert", (device, alert, email) => {
       console.log("[%s] alert", device.serial());
     })
     .listen(config).then(() => console.log("LinQ Ready!"))
     .catch(e => console.log(e);

config.js

.. code-block:: javascript

   let fs = require("fs");
   const env = process.env.NODE_ENV || "dev";
   
   // Read a potential TLS key from the enviorment
   let key;
   try {
     key = fs.readFileSync(process.env.TLS_KEY || "../enviorments/unsafe-key.pem");
     console.log("TLS KEY FOUND");
   } catch (e) {
     console.log("No TLS key found");
   }
   
   // Read a potential TLS cert from the enviorment
   let cert;
   try {
     cert = fs.readFileSync(
       process.env.TLS_CERT || "../enviorments/unsafe-cert.pem"
     );
     console.log("TLS CERT FOUND");
   } catch (e) {
     console.log("No TLS cert found");
   }
   
   const dev = {
     key: key,
     cert: cert,
     tcp: parseInt(process.env.TCP_PORT) || 3380,
     tcps: parseInt(process.env.TCPS_PORT) || 33433
   };
   
   const test = {
     key: key,
     cert: cert,
     tcp: parseInt(process.env.TCP_PORT) || 3380,
     tcps: parseInt(process.env.TCPS_PORT) || 33433
   };
   
   const config = { dev, test };
   
   module.exports = config[env];

