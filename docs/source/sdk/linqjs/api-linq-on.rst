.. _ref-linq-on:

linq.on
=======

.. code-block:: javascript

  linq.on(alert_type, alert_callback): Linq

The linq.on() method will set up a callback for the nodejs event emitter. The following alerts and callback signatures are described below.

Parameters
----------
.. table::
  :align: left

  ============== ======== ===========
  Parameter      type     description
  ============== ======== ===========
  alert_type     string   Config object or Port number
  alert_callback function callback function executed when alert of type is emitted
  ============== ======== ===========

Alerts
------

.. table::
  :align: left

  ========= =============
  Parameter callback args
  ========= =============
  new       :ref:`ref-device`
  alert     :ref:`ref-device`, :ref:`ref-protocol-packet-alert`
  timeout   :ref:`ref-device`
  error     string       
  ========= =============

1. new

   * Emits when a new device is connected to linq

   * A :ref:`ref-device` context is passed to callers event handler

2. alert

   * Emits when a a device has generated an alert

   * A device context and the alert context is passed to the callers event handler

3. timeout

   * Emites when a device has not been heard from in a period of time specified.

4. error

   * Emits when linq throws and error

   * A string describing the error is passed to the callers event handler

Example Event Handler (New Device)
----------------------------------

.. code-block:: javascript

  linq.on("new", (device) => {
    console.log(`A new device is connected to linq!`);
    console.log(`Serial Number: ${device.serial()}`);
  });


Example Event Handler (Alert from Device)
-----------------------------------------

.. code-block:: javascript

  linq.on("alert", (device, alert) => {
    console.log(`An alert from ${device.serial()}!`);
    console.log("who: %s\nwhat: %s\nwhen: %s\n", alert.who, alert.what, alert.when);
  });

* see also

  * :ref:`ref-protocol-packet-alert`
