.. _ref-linq-devices:

linq.devices
============

.. code-block:: javascript

  linq.devices: {[x:string]: Device }

The linq.device object contains a map of devices that are indexed by the serial number of the device. The device context provides methods to interact further with the device.

* see also
    
  * :ref:`ref-device`

Example
-------

.. code-block:: javascript
  
  app.get('/api-v1/device/:sid', function(req, res, next) {
    let device = linq.device(req.params.sid);
    if (!device) {
      res.status(404).send("device not found");
    } else {
      res.locals._device = device;
      next();
    }
  });
