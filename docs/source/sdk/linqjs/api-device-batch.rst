.. _ref-device-batch:

device.batch
==============

.. code-block:: javascript

  device.batch(request: RequestParams[]): Promise;

The device.batch method will append multiple requests to the queue and return a promise when all requests are fullfilled by the device. For a list of the API, see the HTTP API section of this document.

Example Batch Request
---------------------

Change the IP address of the device and save settings into ROM

.. code-block:: javascript

  device.batch([{
    path: "ATX/network/ip",
    data: {ip: "192.168.0.111"}
  },{
    path: "ATX/exe/save",
    data: {save:1}
  }).catch(e => console.log(e));

* see also

  * :ref:`ref-device-request-params`

  * :ref:`ref-device-request-errors`
