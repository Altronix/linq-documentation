.. _ref-device-serial:

device.serial
=============

.. code-block:: javascript

  device.serial(): string;

The device.serial() method will return a serial number string of the device.

Parameters
----------

None.
