.. _ref-device-update:

device.update
=============

.. code-block:: javascript

  device.update(update: DashboardPayload): Promise;

The device.update method will update the firmware of the device. Dashboard updates can be found at https://linq.altronix.com

Example Firmware Update
-----------------------

.. code-block:: javascript

  let result = false;

  device.update(update)
    .then(resp => result = resp)
    .catch( err => result = err);

  (function _log(){
    if (result) return;
    console.log(`${device.pending()} requests remaining`);
    setTimeout(_log, 1000);
  })()
