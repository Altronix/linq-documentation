.. _ref-linq-shutdown:

linq.shutdown
=============

.. code-block:: javascript

  linq.shutdown(): void

The linq.shutdown() method will free the resources from linq.listen() and is required to close the application gracefully.

Parameters
----------

None
