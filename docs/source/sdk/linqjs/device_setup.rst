Enable your LinQ product to connect to LinQJS
---------------------------------------------

Connect your Altronix LinQ enabled product to your local area network. Using your browser, connect to your Altronix LinQ enabled product and navigate to Settings->Cloud->TCP/IP. Fill out the form to enable your product to connect to LinQJS.

(see form image below)

.. image:: cloud-tcp-setup.png

==========  ==========
Field       Description
==========  ==========
IP Address  IP address of the host machine running linqjs.
Port        Port number where the linqjs application is listening.
Use TLS     If the linqjs application is using tls, select the "Use TLS" property.
Enabled     To enable the device to establish a connection to the host machine running linqjs, select "Enabled" property.
==========  ==========

Enable security
---------------

To ensure a secure connection between your device and the LinQJS host machine, you must setup a secure connection with TLS. The host machine has a public and private key pair. The device must be installed with the public key only.

(see form image below)

.. image:: cloud-cert-setup.png

============  ==========
Field         Description
============  ==========
Cloud Upload  Upload the public key (.PEM), then click submit.
============  ==========
