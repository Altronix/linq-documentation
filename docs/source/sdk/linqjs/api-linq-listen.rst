.. _ref-linq-listen:

linq.listen
===========

.. code-block:: javascript

  linq.listen(config: number | LinQConfig): Promise

The linq.listen() method takes a port number parameter, or a configuration object. To listen with TLS termination for the embedded devices you must use the configuration object.

The linq.listen() method will create a socket for the ZMTP protocol and listen for incoming device connections. linq.listen() will return a promise when the socket is ready.  This is typically the first function called to start the linqjs module.

* see also

  * :ref:`ref-linq-on` to listen for incoming alerts

  * :ref:`ref-linq-devices` to get an object of connected devices indexed by serial number

  * :ref:`ref-linq-shutdown` to clean up and close LinQJS module

Parameters
----------

.. table::
  :align: left

  ========= =================== ===========
  Parameter type                description
  ========= =================== ===========
  config    number | LinQConfig Config object or Port number
  ========= =================== ===========


.. _ref-linq-config:

LinQConfig configuration object

.. table::
  :align: left

  ==== ====== =================== =============================
  Prop type   required            description
  ==== ====== =================== =============================
  cert string Only when using TLS A pem format certificate 
  key  string Only when using TLS A pem format key 
  tcp  number true                A pem format port number for linq listener
  tcps number Only when using TLS A pem format port number for linq tls termination
  ==== ====== =================== =============================

Example: Listen with TLS termination
------------------------------------

.. code-block:: javascript

  linq.listen({
     key = fs.readFileSync(process.env.TLS_KEY),
     cert = fs.readFileSync(process.env.TLS_CERT),
     tcp = 4590,
     tcps = 4591
  });

Example: Listen unsecure
------------------------

.. code-block:: javascript

  linq.listen(4590);

