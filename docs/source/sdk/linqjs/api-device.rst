.. _ref-device:

Device
======

The main device context to. One instance is created per each device connected to linqjs

Properties
----------

.. table::
  :align: left
 
  ========================= ====== =====================
  property                  type   description
  ========================= ====== =====================
  uptime                    unix
  lastSeen                  unix
  data                      any
  :ref:`ref-device-product` method return product string of device
  :ref:`ref-device-serial`  method return serial number of device
  :ref:`ref-device-request` method send a request to the device
  :ref:`ref-device-batch`   method send multiple requests to the device
  :ref:`ref-device-update`  method update the device
  :ref:`ref-device-pending` method return how many requests are pending to the device
  ========================= ====== =====================
